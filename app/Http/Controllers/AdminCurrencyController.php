<?php

namespace App\Http\Controllers;

use App\Services\CurrencyPresenter;
use App\Services\CurrencyRepositoryInterface;
use Illuminate\Http\Request;

class AdminCurrencyController extends Controller
{
    private $repository;

    public function __construct(CurrencyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @param CurrencyRepositoryInterface $repository
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = CurrencyPresenter::presentArray($this->repository->findAll());
        return response()->json($currencies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param CurrencyRepositoryInterface $repository
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currency = CurrencyPresenter::presentObject($request->input());
        $this->repository->save($currency);
        return response()->json($currency);
    }

    /**
     * Display the specified resource.
     *
     * @param CurrencyRepositoryInterface $repository
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $currency = $this->repository->findById($id);
        return $currency ? response()->json(CurrencyPresenter::present($currency)) :
            response()->json(CurrencyPresenter::presentErrorNotFound($id), 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentCurrency = $this->repository->findById($id);
        if ($currentCurrency) {
            $newCurrency = CurrencyPresenter::presentUpdated($currentCurrency, $request->input());
            $this->repository->update($currentCurrency, $newCurrency);
            return response()->json(CurrencyPresenter::presentMessage("Currency with id $id was successfully updated."), 200);
        } else {
            return response()->json(CurrencyPresenter::presentErrorNotFound($id), 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currency = $this->repository->findById($id);
        if ($currency){
            $this->repository->delete($currency);
            return response()->json(CurrencyPresenter::presentMessage("Currency with id $id was successfully deleted."), 200);
        } else {
            return response()->json(CurrencyPresenter::presentErrorNotFound($id), 404);
        }
    }

    public function all()
    {
        $currencies = CurrencyPresenter::presentArray($this->repository->findAll());
        return view('currencies', ['currencies' => $currencies]);
    }
}
