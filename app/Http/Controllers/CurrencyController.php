<?php

namespace App\Http\Controllers;

use App\Services\CurrencyPresenter;
use App\Services\CurrencyRepositoryInterface;

class CurrencyController extends Controller
{
    private $repository;

    public function __construct(CurrencyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $activeCurrencies = CurrencyPresenter::presentArray($this->repository->findActive());
        return response()->json($activeCurrencies);
    }

    public function show(int $id)
    {
        $currency = $this->repository->findById($id);
        return $currency ? response()->json(CurrencyPresenter::present($currency)) :
            response()->json(CurrencyPresenter::presentErrorNotFound($id), 404);
    }
}
