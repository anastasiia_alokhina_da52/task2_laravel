<?php

namespace App\Services;

class CurrencyPresenter
{
    public static function present(Currency $currency): array
    {
        $presentedCurrency['id'] = $currency->getId();
        $presentedCurrency['name'] = $currency->getName();
        $presentedCurrency['short_name'] = $currency->getShortName();
        $presentedCurrency['actual_course'] = $currency->getActualCourse();
        $presentedCurrency['actual_course_date'] = ($currency->getActualCourseDate())->format('Y-m-d H:i:s');
        $presentedCurrency['active'] = $currency->isActive();
        return $presentedCurrency;
    }

    public static function presentArray(array $currencies): array
    {
        $presentedCurrencies = [];
        foreach ($currencies as $currency){
            $presentedCurrencies[] = self::present($currency);
        }
        return $presentedCurrencies;
    }

    public static function presentObject(array $currency): Currency
    {
        $currency = new Currency(
            $currency['id'] ?? 0,
            $currency['name'],
            $currency['short_name'],
            $currency['actual_course'],
            \DateTime::createFromFormat('Y-m-d H-i-s', $currency['actual_course_date']),
            $currency['active']
        );
        return $currency;
    }

    public static function presentUpdated(Currency $currentCurrency, array $updatedFields): Currency
    {
        $allFields = CurrencyPresenter::present($currentCurrency);
        foreach ($allFields as $field => $value) {
            if (array_key_exists($field, $updatedFields)) {
                $allFields[$field] = $updatedFields[$field];
            }
        }
        return CurrencyPresenter::presentObject($allFields);
    }

    public static function presentErrorNotFound(int $id): array
    {
        return [
            'error' => "Currency with id $id does not exist."
        ];
    }

    public static function presentError(string $error): array
    {
        return [
            'error' => $error
        ];
    }

    public static function presentMessage(string $message): array
    {
        return [
            'message' => $message
        ];
    }
}