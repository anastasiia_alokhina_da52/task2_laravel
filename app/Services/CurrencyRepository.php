<?php


namespace App\Services;


class CurrencyRepository implements CurrencyRepositoryInterface
{
    private $currencies;

    public function __construct(array $currencies)
    {
        $this->currencies = $currencies;
    }

    public function findAll(): array
    {
        return $this->currencies;
    }

    public function findActive(): array
    {
        $activeCurrencies = [];
        foreach ($this->currencies as $currency){
            if ($currency->isActive()) {
                $activeCurrencies[] = $currency;
            }
        }
        return $activeCurrencies;
    }

    public function findById(int $id): ?Currency
    {
        foreach ($this->currencies as $currency){
            if ($currency->getId() === $id){
                return $currency;
            }
        }
        return null;
    }

    public function save(Currency $currency): void
    {
        $this->currencies[] = $currency;
    }

    public function delete(Currency $currency): void
    {
        $result = array_search($currency, $this->currencies);
        if ($result !== false) {
            unset($this->currencies[$result]);
        }
    }

    public function update(Currency $currentCurrency, Currency $newCurrency): void
    {
        $this->delete($currentCurrency);
        $this->save($newCurrency);
    }

    public function getLastId(): int
    {
        $lastCurrency = end($this->currencies);
        return $lastCurrency->getId();
    }
}