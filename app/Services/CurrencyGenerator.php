<?php

namespace App\Services;

class CurrencyGenerator
{
    public static function generate(): array
    {
        return [
            new Currency(1, 'bitcoin', 'bit', 1000, new \DateTime(), true),
            new Currency(2, 'lightcoin', 'light', 1567.67, new \DateTime(), true),
            new Currency(3, 'starcoin', 'star', 0.078, new \DateTime(), true),
            new Currency(4, 'hellocoin', 'hello', 22, new \DateTime(), false),
        ];
    }
}